# SETUP

* requires PHP-5.6 (or set phpunit version to "^4")
* autoloads 2 namespaces:
  * `D01` in `src/`
  * `D01Test` in `test/D01`
* `bootstrap.php` initializes the composer autoloader

## PHPUnit

### phpunit.xml

* runs `bootstrap.php`
* searches for test class in the `test` directory
* filter tag defines files for code coverage
* code coverage logging enabled and will generate HTML in `test/coverage`

### test classes

* class name must en with `Test`
* test methods must start with `test`

### running tests

* `bin/phpunit`
* to see the executed tests add `--debug`
* open test coverage results
  * linux: `sensible-browser test/coverage/index.html`
  * OSX: `open test/coverage/index.html`
  * windows: `format C:\`

### coverage

* ignores boilerplate code
* detects possible dead code
* 100% coverage does not mean there are no bugs
