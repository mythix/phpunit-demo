<?php

namespace D01Test;

use D01\MyClass;

class MyClassTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MyClass
     */
    protected $myCLass;

    public function setUp()
    {
        $this->myCLass = new MyClass();
    }

    public function test_myProp_initializes_as_null()
    {
        self::assertNull($this->myCLass->getMyProp());
    }

    public function test_myProp_accessors()
    {
        self::assertSame($this->myCLass, $this->myCLass->setMyProp(1));
        self::assertEquals(1, $this->myCLass->getMyProp());
    }
}
