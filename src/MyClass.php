<?php

namespace D01;

class MyClass
{
    /**
     * @var int
     */
    private $myProp;

    /**
     * @return int
     */
    public function getMyProp()
    {
        return $this->myProp;
    }

    /**
     * @param int $myProp
     * @return $this
     */
    public function setMyProp($myProp)
    {
        $this->myProp = $myProp;
        return $this;
    }
}
